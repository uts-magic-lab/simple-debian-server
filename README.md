# Simple Debian Server

Daemon to automatically update the `Packages.gz` file when a debian repository.

### Usage

Mount your public web directory at `/var/www/debian`. This daemon will update the `Packages.gz` file whenever files are added.

```bash
docker pull magiclab/simple-debian-server
docker run -v "/var/www/html/debian:/var/www/debian" magiclab/simple-debian-server
```

### Development

```bash
docker build -t magiclab/simple-debian-server .
```
