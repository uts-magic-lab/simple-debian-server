#!/bin/sh -eu

mkdir -p /var/www

cd "${1:-/var/www}"

mkdir -p ./debian

# update Packages.gz once when starting
dpkg-scanpackages ./debian /dev/null | gzip -9c > ./debian/Packages.gz

inotifywait -m ./debian -e create -e moved_to | \
while read path action file; do
    # update Packages.gz each time a new file is added
    dpkg-scanpackages ./debian /dev/null | gzip -9c > ./debian/Packages.gz
    # make .deb files read-only
    chmod -w ./debian/*.deb
done
