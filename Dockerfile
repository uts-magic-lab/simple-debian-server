FROM debian:latest

RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
    dpkg-dev \
    gzip \
    inotify-tools && \
rm -rf /var/lib/apt/lists/*

ADD assets /

CMD ["/usr/local/bin/simple-debian-server.sh"]
